from django.urls import path

from versutus.community.views import (
    communities_view,
    opportunities_view,
    shimmerians_view,
)

app_name = "crew3"

urlpatterns = [
    path("communities/", view=communities_view, name="communities"),
    path("communities/shimmerians/", view=shimmerians_view, name="shimmerians"),
    path("opportunities/", view=opportunities_view, name="opportunities"),
]
