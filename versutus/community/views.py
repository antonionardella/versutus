import requests
from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views.generic import TemplateView

from versutus.tangle.models import EcosystemOpportunities

User = get_user_model()


def get_crew3_api_data(subdomain, x_api_key, page_number):
    api_url = (
        f"https://api.crew3.xyz/communities/{subdomain}/leaderboard?page={page_number}"
    )
    headers = {"x-api-key": x_api_key}
    response = requests.get(api_url, headers=headers)
    return response.json()


def opportunities_view(request):
    # Retrieve all non-archived opportunities from the database
    opportunities = EcosystemOpportunities.objects.filter(archived=False)

    # Count the number of opportunities
    count = opportunities.count()

    # Pass the opportunities and count to the template context
    context = {
        "opportunities": opportunities,
        "count": count,
    }

    return render(request, "opportunities.html", context)


@login_required
def shimmerians_view(request):
    subdomain = "shimmerians"
    x_api_key = "72812apRGCSGJsgI99NIoAzcAeF"

    # Retrieve the page number from the request query string
    page_number = int(request.GET.get("page", 1))
    users = get_crew3_api_data(subdomain, x_api_key, page_number)

    for entry in users["leaderboard"]:
        if entry.get("avatar"):
            avatar_url = entry["avatar"]
            if avatar_url.startswith("https://cdn.discordapp.com/"):
                entry["avatar"] = avatar_url
            elif avatar_url.startswith("public/"):
                entry["avatar"] = (
                    "https://crew3-production.s3.eu-west-3.amazonaws.com/public/"
                    + avatar_url[7:]
                )
            else:
                entry["avatar"] = "https://crew3.xyz" + avatar_url
    # Add the total number of users and total pages to the context dictionary
    total_users = users["totalUsers"]
    total_pages = users["totalPages"]
    context = {
        "users": users["leaderboard"],
        "total_users": total_users,
        "total_pages": total_pages,
        "current_page": page_number,
    }
    return render(request, "shimmerians.html", context=context)


class Communities(LoginRequiredMixin, TemplateView):
    model = User
    template_name = "leaderboard.html"


communities_view = Communities.as_view()
