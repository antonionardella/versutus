from allauth.socialaccount.models import SocialAccount
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, RedirectView, UpdateView

User = get_user_model()


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    slug_field = "username"
    slug_url_kwarg = "username"

    def get_user_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        user = self.get_object()
        social_account = SocialAccount.objects.filter(user=user).first()
        user_uid = social_account.uid
        print(f"user_uid: {user_uid}")
        # user_guilds = get_user_guilds(user_id=user_uid)
        # print(f"user_guilds: {user_guilds}")
        # system_guilds = get_system_guilds()

        # setup_status = False
        # guilds_setup_status = []
        # for user_guild in user_guilds:
        #     if str(user_guild.id) in str(system_guilds):
        #         try:
        #             setup_status = get_user_setup_status(user_id=user_id, guild_id=user_guild.id)
        #             if setup_status:
        #              guilds_setup_status.append({"id": user_guild.id, "guild_name": \
        #       user_guild.name, "icon": user_guild.icon, "setup_status": setup_status})
        #         except:
        #             continue

        # available_guilds = [{"id": guild["id"], "name": guild["guild_name"], \
        # "icon": guild["icon"]} for guild in guilds_setup_status if guild.get("setup_status", True)]
        # number_of_available_guilds = len(available_guilds)
        if social_account:
            context["social_account"] = social_account
        # Add new context to existing context
        context["user_uid"] = user_uid
        # context["user_guilds"] = user_guilds
        # context["number_of_available_guilds"] : number_of_available_guilds
        print(f"context: {context}")
        return context


user_detail_view = UserDetailView.as_view()


class UserUpdateView(LoginRequiredMixin, SuccessMessageMixin, UpdateView):
    model = User
    fields = ["name"]
    success_message = _("Information successfully updated")

    def get_success_url(self):
        assert (
            self.request.user.is_authenticated
        )  # for mypy to know that the user is authenticated
        return self.request.user.get_absolute_url()

    def get_object(self):
        return self.request.user


user_update_view = UserUpdateView.as_view()


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):
        return reverse("users:detail", kwargs={"username": self.request.user.username})


user_redirect_view = UserRedirectView.as_view()
