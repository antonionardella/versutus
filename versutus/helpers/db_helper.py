from versutus.tangle.models import DiscordGuild, DiscordUser, DiscordUserSetup

# def get_active_guilds():
#     guilds = DiscordGuild.objects.all()
#     return guilds.guild_id

# def get_user_setup_status(user_id, guild_id):
#     user = DiscordUser.objects.get(user_id=user_id)
#     guild = DiscordGuild.objects.get(guild_id=guild_id)
#     user_setup = DiscordUserSetup.objects.get(user=user, guild=guild)
#     return user_setup.setup_completed

# def get_system_guilds():
#     guilds = [guild.guild_id for guild in DiscordGuild.objects.all()]
#     return guilds


def get_active_guilds():
    return DiscordGuild.objects.all()


def get_system_guilds():
    active_guilds = get_active_guilds()
    return [guild.guild_id for guild in active_guilds]


def get_user_setup_status(user_id, guild_id):
    user = DiscordUser.objects.get(user_id=user_id)
    guild = DiscordGuild.objects.get(guild_id=guild_id)
    user_setup = DiscordUserSetup.objects.get(user=user, guild=guild)
    return user_setup.setup_completed


def get_user_guilds(user_id):
    user_guilds = DiscordUserSetup.objects.filter(user_id=user_id)
    return [guilds.guild for guilds in user_guilds]


def get_user_setup_in_guilds(user_id):
    user_guilds = get_user_guilds(user_id)
    user_setup_in_guilds = {}
    for guild in user_guilds:
        user_setup_in_guilds[guild.guild_id] = get_user_setup_status(
            user_id, guild.guild_id
        )
    return user_setup_in_guilds
