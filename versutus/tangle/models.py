from django.db import models

# Create your models here.


class DiscordUser(models.Model):
    user_id = models.BigIntegerField(primary_key=True)
    user_name = models.CharField(max_length=255)
    shimmer_address_mainnet = models.CharField(max_length=63)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class DiscordGuild(models.Model):
    guild_id = models.BigIntegerField(primary_key=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class DiscordUserConnections(models.Model):
    connection_type = models.CharField(max_length=255)
    connection_username = models.CharField(max_length=255)
    connection_verified = models.BooleanField()
    connection_visibility = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(
        DiscordUser, on_delete=models.CASCADE, related_name="connections"
    )


class NftCampaign(models.Model):
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="nft_campaigns"
    )
    nft_campaign_name = models.CharField(max_length=255)
    nft_campaign_nft_amount = models.IntegerField()
    nft_campaign_manager_name = models.CharField(max_length=255)
    nft_campaign_manager_id = models.BigIntegerField()
    nft_campaign_discord_role = models.CharField(max_length=255)
    nft_campaign_discord_role_id = models.BigIntegerField()
    nft_campaign_discord_role_eligible = models.BooleanField()
    nft_campaign_active = models.BooleanField(null=True, blank=True)
    nft_campaign_archived = models.BooleanField(null=True, blank=True)
    nft_campaign_shimmer_mainnet_address = models.CharField(max_length=63, blank=True)


class DiscordGuildRoles(models.Model):
    role_id = models.BigIntegerField(primary_key=True)
    role_name = models.CharField(max_length=255)
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="roles"
    )


class NftEligibleDiscordGuildRoles(models.Model):
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="eligible_roles"
    )
    role = models.ForeignKey(
        DiscordGuildRoles, on_delete=models.CASCADE, related_name="eligible_roles"
    )
    campaign = models.ForeignKey(
        NftCampaign, on_delete=models.CASCADE, related_name="eligible_roles"
    )


class DiscordUserSetup(models.Model):
    user = models.ForeignKey(
        DiscordUser, on_delete=models.CASCADE, related_name="setups"
    )
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="setups"
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    setup_completed = models.BooleanField(default=False)


class DiscordUserRoles(models.Model):
    user = models.ForeignKey(
        DiscordUser, on_delete=models.CASCADE, related_name="roles"
    )
    role = models.ForeignKey(
        DiscordGuildRoles, on_delete=models.CASCADE, related_name="users"
    )
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="user_roles"
    )


class DiscordUserShimmerAddresses(models.Model):
    user = models.ForeignKey(
        DiscordUser, on_delete=models.CASCADE, related_name="shimmer_addresses"
    )
    shimmer_mainnet_address = models.CharField(max_length=63, blank=True)
    shimmer_devnet_address = models.CharField(max_length=63, blank=True)
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="shimmer_addresses"
    )


class NftSent(models.Model):
    user_id = models.BigIntegerField()
    guild_id = models.BigIntegerField()
    role_id = models.BigIntegerField()
    sent_at = models.DateTimeField(auto_now=True)
    campaign = models.ForeignKey(
        NftCampaign, on_delete=models.CASCADE, related_name="sent_nfts"
    )
    is_sent = models.BooleanField()


class BlacklistedUser(models.Model):
    user = models.ForeignKey(
        DiscordUser, on_delete=models.CASCADE, related_name="blacklisted_users"
    )
    guild = models.ForeignKey(
        DiscordGuild, on_delete=models.CASCADE, related_name="blacklisted_users"
    )


class IotaHexAddress(models.Model):
    address = models.CharField(max_length=64, primary_key=True)
    balance = models.BigIntegerField()


class IotaTopAddress(models.Model):
    address = models.CharField(max_length=64, primary_key=True)
    balance = models.CharField(max_length=20)


class IotaDistribution(models.Model):
    amounts = models.CharField(max_length=20)
    addresses = models.IntegerField()
    sum_balances = models.CharField(max_length=11)
    percent_addresses = models.CharField(max_length=11)
    percent_supply = models.CharField(max_length=11)


class ShimmerHexAddress(models.Model):
    address = models.CharField(max_length=80, primary_key=True)
    balance = models.BigIntegerField()


class ShimmerTopAddress(models.Model):
    address = models.CharField(max_length=80, primary_key=True)
    balance = models.CharField(max_length=20)


class ShimmerDistribution(models.Model):
    amounts = models.CharField(max_length=20)
    addresses = models.IntegerField()
    sum_balances = models.CharField(max_length=11)
    percent_addresses = models.CharField(max_length=11)
    percent_supply = models.CharField(max_length=11)


class EcosystemOpportunities(models.Model):
    thread_id = models.BigIntegerField(primary_key=True)
    name = models.CharField(max_length=200)
    category = models.CharField(max_length=100)
    archived = models.BooleanField(default=False)
    locked = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.name} ({self.category})"
