import csv
import logging

import requests
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import Group
from django.shortcuts import render
from django.views.generic import TemplateView
from iota_client import (
    IotaClient as ShimmerClient,  # Imported as ShimmerClient to prevent confusion
)

from versutus.tangle.forms import SmrAddressForm
from versutus.tangle.models import (
    IotaDistribution,
    IotaTopAddress,
    ShimmerDistribution,
    ShimmerTopAddress,
)

logger = logging.getLogger(__name__)

User = get_user_model()


def is_user_in_nft_manager_group(user):
    try:
        nft_manager_group = Group.objects.get(name="NFT Managers")
    except Group.DoesNotExist:
        return False

    return nft_manager_group in user.groups.all()


def get_shimmer_richlist_from_chronicle_api(richlist_results):
    api_url = f"https://chronicle.shimmer.network/api/explorer/v2/ledger/richest-addresses?top={richlist_results}"
    response = requests.get(api_url)
    return response.json()


def iota(request):
    if request.user.is_authenticated:
        richlist_results = 500
    else:
        richlist_results = 20

    context = {
        "iota_distribution": IotaDistribution.objects.all(),
        "iota_richtlist": IotaTopAddress.objects.all()[:richlist_results],
        "richlist_results": richlist_results,
    }
    return render(request, "iota.html", context=context)


def shimmer(request):
    if request.user.is_authenticated:
        richlist_results = 500
    else:
        richlist_results = 20

    chronicle_api_richlist = get_shimmer_richlist_from_chronicle_api(richlist_results)
    for chronicle_api_entry in chronicle_api_richlist["top"]:
        shimmer_api_address = chronicle_api_entry["address"]
        shimmer_api_balance = chronicle_api_entry["balance"]
    shimmer_api_ledger_index = chronicle_api_richlist["ledgerIndex"]

    context = {
        "shimmer_distribution": ShimmerDistribution.objects.all(),
        "shimmer_richtlist": ShimmerTopAddress.objects.all()[:richlist_results],
        "richlist_results": richlist_results,
        "shimmer_api_ledger_index": shimmer_api_ledger_index,
        "shimmer_api_address": shimmer_api_address,
        "shimmer_api_balance": shimmer_api_balance,
    }
    return render(request, "shimmer.html", context=context)


def verify_content(csv_content):
    invalid_rows = []
    csv_reader = csv.reader(csv_content.splitlines())
    headers = next(csv_reader)
    if "answer" not in headers:
        invalid_rows.append((" ", "INVALID CSV FILE", " "))
        return invalid_rows

    address_index = headers.index("answer")
    if "Name" in headers:
        name_index = headers.index("Name")
    else:
        return invalid_rows
    for i, row in enumerate(csv_reader):
        logger.debug(row)
        if not verify_shimmer_address(row[address_index]):
            invalid_rows.append((row[name_index], row[address_index], i + 2))
    return invalid_rows


def verify_shimmer_address(address):
    logger.debug(f"Verifying address: {address}")
    shimmer_client = ShimmerClient()
    return shimmer_client.is_address_valid(address)


class Tools(LoginRequiredMixin, TemplateView):
    model = User
    template_name = "tools.html"


tools_view = Tools.as_view()


class Crew3Tools(LoginRequiredMixin, TemplateView):
    model = User
    template_name = "crew3_tools.html"

    def post(self, request, *args, **kwargs):
        if request.method == "POST":
            csv_file = request.FILES["file"]
            csv_content = csv_file.read().decode("utf-8")
            invalid_rows = verify_content(csv_content)
            if invalid_rows:
                context = {
                    "invalid_rows": invalid_rows,
                }
                return render(request, "csv_verify.html", context=context)
            else:
                return render(request, "csv_success.html")
        return render(request, "csv_upload.html")


crew3_tools_view = Crew3Tools.as_view()


class ShimmerAddress(LoginRequiredMixin, TemplateView):
    model = User
    template_name = "smr_address.html"
    form_class = SmrAddressForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None)
        return self.render_to_response({"form": form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST or None)
        if form.is_valid():
            address = form.cleaned_data.get("address")
            if verify_shimmer_address(address):
                context = {"address": address}
                if address.startswith("rms1"):
                    context["testnet"] = True
                # Address is valid, do something
                return render(request, "smr_address_success.html", context=context)
            else:
                form.add_error("address", "Invalid address")
                context = {"address": address}
                return render(request, "smr_address_verify.html", context=context)
        return self.render_to_response({"form": form})


smr_address_view = ShimmerAddress.as_view()
