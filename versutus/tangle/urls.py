from django.urls import path

from versutus.tangle.views import (
    crew3_tools_view,
    iota,
    shimmer,
    smr_address_view,
    tools_view,
)

app_name = "tangle"

urlpatterns = [
    path("iota/", view=iota, name="iota"),
    path("shimmer/", view=shimmer, name="shimmer"),
    path("tools/", view=tools_view, name="tools"),
    path("tools/crew3_smr/", view=crew3_tools_view, name="crew3_tools"),
    path("tools/smr_address/", view=smr_address_view, name="smr_address"),
]
