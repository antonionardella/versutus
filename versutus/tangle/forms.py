from django import forms


class SmrAddressForm(forms.Form):
    address = forms.CharField(
        max_length=64,
        widget=forms.TextInput(
            attrs={"maxlength": 64, "placeholder": "Enter SMR address"}
        ),
        required=True,
    )
