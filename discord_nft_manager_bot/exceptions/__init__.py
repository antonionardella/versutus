""""
Copyright © Krypton 2022 - https://github.com/kkrypt0nn (https://krypton.ninja)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""

from discord.ext import commands


class UserBlacklisted(commands.CheckFailure):
    """
    Thrown when a user is attempting something, but is blacklisted.
    """

    def __init__(self, message="User is blacklisted!"):
        self.message = message
        super().__init__(self.message)


class UserSetup(commands.CheckFailure):
    """
    Thrown when a user is attempting setup, but is set up already.
    """

    def __init__(
        self,
        message="ℹ️ Your user is already set up.\n You can use `/profile` commands to edit your profile data.",
    ):
        self.message = message
        super().__init__(self.message)


class UserNotOwner(commands.CheckFailure):
    """
    Thrown when a user is attempting something, but is not an owner of the bot.
    """

    def __init__(self, message="User is not an owner of the bot!"):
        self.message = message
        super().__init__(self.message)


class UserNotCampaignManager(commands.CheckFailure):
    """
    Thrown when a user is attempting something, but is a NFT campaign manager.
    """

    def __init__(self, message="User is not a NFT campaign manager!"):
        self.message = message
        super().__init__(self.message)


class NotShimmerMainnetAddress(commands.CheckFailure):
    """
    Thrown when a user is submitting Shimmer Mainnet address, but the input is not valid.
    """

    def __init__(self, message="Shimmer Address is not valid"):
        self.message = message
        super().__init__(self.message)


class UserHasNoShimmerMainnetAddress(commands.CheckFailure):
    """
    Thrown when a user is submitting Shimmer Mainnet address, but the input is not valid.
    """

    def __init__(self, message="User has no Shimmer mainnet address in the DB"):
        self.message = message
        super().__init__(self.message)
