"""
Copyright © Krypton 2022 - https://github.com/kkrypt0nn (https://krypton.ninja)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""

import asyncio
import multiprocessing
import os
import platform
import random
import time
import traceback

import discord
import environ
import exceptions
from discord.ext import commands, tasks
from discord.ext.commands import Bot, Context
from helpers import db_manager, embed_and_messages, iota_token_data, shimmer_token_data
from helpers.logger import logger

env = environ.Env()

"""
Setup bot intents (events restrictions)
For more information about intents, please go to the following websites:
https://discordpy.readthedocs.io/en/latest/intents.html
https://discordpy.readthedocs.io/en/latest/intents.html#privileged-intents


Default Intents:
intents.bans = True
intents.dm_messages = True
intents.dm_reactions = True
intents.dm_typing = True
intents.emojis = True
intents.emojis_and_stickers = True
intents.guild_messages = True
intents.guild_reactions = True
intents.guild_scheduled_events = True
intents.guild_typing = True
intents.guilds = True
intents.integrations = True
intents.invites = True
intents.messages = True # `message_content` is required to get the content of the messages
intents.reactions = True
intents.typing = True
intents.voice_states = True
intents.webhooks = True

Privileged Intents (Needs to be enabled on developer portal of Discord), please use them only if you need them:
intents.members = True
intents.message_content = True
intents.presences = True
"""

intents = discord.Intents.default()

"""
Uncomment this if you don't want to use prefix (normal) commands.
It is recommended to use slash commands and therefore not use prefix commands.

If you want to use prefix commands, make sure to also enable the intent below in the Discord developer portal.
"""
# intents.message_content = True

bot = Bot(
    command_prefix=commands.when_mentioned_or(env("PREFIX")),
    intents=intents,
    help_command=None,
)


def background_task():
    logger.info("Starting background tasks for the DLT ledger data")
    asyncio.run(iota_token_data.main())
    asyncio.run(shimmer_token_data.main())
    time.sleep(24 * 60 * 60)
    background_task()


def run_bot():
    asyncio.run(load_cogs())
    asyncio.run(embed_and_messages.create_empty_embed_and_messages())
    bot.run(env("DISCORD_TOKEN"))


"""
Create a bot variable to access the config file in cogs so that you don't need to import it every time.
"""


@bot.event
async def on_ready() -> None:
    """
    The code in this even is executed when the bot is ready
    """
    for guild in bot.guilds:
        guild_id = guild.id
    logger.info(f"Logged in as {bot.user.name}")
    logger.info(f"Logged in on Guild ID: {guild_id}")
    logger.info(f"discord.py API version: {discord.__version__}")
    logger.info(f"Python version: {platform.python_version()}")
    logger.info(f"Running on: {platform.system()} {platform.release()} ({os.name})")
    logger.info("-------------------")
    await db_manager.add_guild_id_to_db(guild_id)
    status_task.start()
    check_new_opportunities.start()
    sync_commands_globally = env.bool("SYNC_COMMANDS_GLOBALLY")
    if sync_commands_globally == "True":
        sync_commands_globally = True
    else:
        sync_commands_globally = False
    sync_commands_globally = bool(sync_commands_globally)
    if sync_commands_globally:
        logger.info("Syncing commands globally...")
        await bot.tree.sync()


@tasks.loop(minutes=5.0)
async def status_task() -> None:
    """
    Setup the game status task of the bot
    """
    statuses = ["with you!", "with Skip!", "with humans!"]
    await bot.change_presence(activity=discord.Game(random.choice(statuses)))


@tasks.loop(hours=12.0)
async def check_new_opportunities() -> None:
    logger.info("Checking for new opportunities...")
    # Add routine to check if the message is an opportunity forum message and if so, add it to the database
    channel_id = env("OPPORTUNITIES_CHANNEL_ID")  # replace with your forum channel id
    try:
        # logger.debug(f"Channel ID: {channel_id}")
        channel = bot.get_channel(int(channel_id))
        # logger.debug(f"Channel: {channel}")
        threads = channel.threads
        # logger.debug(f"Threads: {threads}")

        # Create EcosystemOpportunities objects for each thread
        opportunities = []
        for thread in threads:
            # Check if thread is already in the database
            # logger.debug(f"Thread ID: {thread.id}")
            if await db_manager.get_opportunity_by_thread_id(thread.id):
                continue

            category = thread.applied_tags[0].name if thread.applied_tags else None
            opportunity = {
                "thread_id": thread.id,
                "name": thread.name,
                "category": category,
                "archived": thread.archived,
            }
            opportunities.append(opportunity)

        # bulk create opportunities in the database
        await db_manager.add_opportunities(opportunities)

        # Remove opportunities from the database that no longer exist in the Discord channel
        db_thread_ids = [
            opportunity.thread_id
            for opportunity in await db_manager.get_all_opportunities()
        ]
        discord_thread_ids = [thread.id for thread in threads]
        missing_thread_ids = set(db_thread_ids) - set(discord_thread_ids)
        if missing_thread_ids:
            await db_manager.remove_opportunities_by_thread_ids(missing_thread_ids)

    except Exception:
        logger.info(traceback.format_exc())


@bot.event
async def on_message(message: discord.Message) -> None:
    """
    The code in this event is executed every time someone sends a message, with or without the prefix

    :param message: The message that was sent.
    """
    if message.author == bot.user or message.author.bot:
        return

    await bot.process_commands(message)


@bot.event
async def on_command_completion(context: Context) -> None:
    """
    The code in this event is executed every time a normal command has been *successfully* executed
    :param context: The context of the command that has been executed.
    """
    full_command_name = context.command.qualified_name
    split = full_command_name.split(" ")
    executed_command = str(split[0])
    if context.guild is not None:
        logger.info(
            f"Executed {executed_command} command in {context.guild.name} (ID: {context.guild.id}) by {context.author} \
                (ID: {context.author.id})"
        )
    else:
        logger.info(
            f"Executed {executed_command} command by {context.author} (ID: {context.author.id}) in DMs"
        )


@bot.event
async def on_command_error(context: Context, error) -> None:
    """
    The code in this event is executed every time a normal valid command catches an error
    :param context: The context of the normal command that failed executing.
    :param error: The error that has been faced.
    """
    if isinstance(error, commands.CommandOnCooldown):
        minutes, seconds = divmod(error.retry_after, 60)
        hours, minutes = divmod(minutes, 60)
        hours = hours % 24
        embed = discord.Embed(
            title="Hey, please slow down!",
            description=f"You can use this command again in {f'{round(hours)} hours' if round(hours) > 0 else ''} \
                {f'{round(minutes)} minutes' if round(minutes) > 0 else ''} \
                    {f'{round(seconds)} seconds' if round(seconds) > 0 else ''}.",
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    elif isinstance(error, exceptions.UserBlacklisted):
        """
        The code here will only execute if the error is an instance of 'UserBlacklisted', which can occur when using
        the @checks.not_blacklisted() check in your command, or you can raise the error by yourself.
        """
        embed = discord.Embed(
            title="Error!",
            description="You are blacklisted from using the bot.",
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    elif isinstance(error, exceptions.UserNotOwner):
        """
        Same as above, just for the @checks.is_owner() check.
        """
        embed = discord.Embed(
            title="Error!",
            description="You are not the owner of the bot!",
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    elif isinstance(error, commands.MissingPermissions):
        embed = discord.Embed(
            title="Error!",
            description="You are missing the permission(s) `"
            + ", ".join(error.missing_permissions)
            + "` to execute this command!",
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    elif isinstance(error, commands.BotMissingPermissions):
        embed = discord.Embed(
            title="Error!",
            description="I am missing the permission(s) `"
            + ", ".join(error.missing_permissions)
            + "` to fully perform this command!",
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    elif isinstance(error, commands.MissingRequiredArgument):
        embed = discord.Embed(
            title="Error!",
            # We need to capitalize because the command arguments have no capital letter in the code.
            description=str(error).capitalize(),
            color=0xE02B2B,
        )
        await context.send(embed=embed)
    raise error


async def load_cogs() -> None:
    """
    The code in this function is executed whenever the bot will start.
    """
    for file in os.listdir("./cogs"):
        if file.endswith(".py"):
            extension = file[:-3]
            try:
                await bot.load_extension(f"cogs.{extension}")
                logger.info(f"Loaded extension '{extension}'")
            except Exception as e:
                exception = f"{type(e).__name__}: {e}"
                logger.info(f"Failed to load extension {extension}\n{exception}")


if __name__ == "__main__":
    # Create processing for the bot and the richlist generation
    process_one = multiprocessing.Process(target=run_bot)
    process_two = multiprocessing.Process(target=background_task)

    # Start the processs
    process_one.start()
    process_two.start()
