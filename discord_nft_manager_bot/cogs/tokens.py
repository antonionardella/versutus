""""
Copyright © antonionardella 2023 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""

import pickle
import traceback

import environ
from discord.ext import commands
from discord.ext.commands import Context
from helpers import checks

env = environ.Env()

bot_reply_channel_id = env("BOT_REPLY_CHANNEL_ID")


# Here we name the cog and create a new class for the cog.
class Tokens(commands.Cog, name="tokens"):
    def __init__(self, bot):
        self.bot = bot

    # Here you can just add your own commands, you'll always need to provide "self" as first parameter.

    @commands.cooldown(1, 3600, commands.BucketType.user)
    @commands.hybrid_command(
        name="iota-richlist",
        description="Gives the Top 5 IOTA addresses",
    )
    # This will only allow non-blacklisted members to execute the command
    @checks.not_blacklisted()
    async def iota_richlist(self, context: Context):
        """
        This command prints an embed with the top 5 IOTA addresses.

        :param context: The application command context.
        """
        global bot_reply_channel_id
        # Do your stuff here

        if context.message.channel.id != int(bot_reply_channel_id):
            await context.send(
                f"This command can only be used in the <#{bot_reply_channel_id}> channel."
            )
            return

        try:
            with open("assets/embed_iota_richlist.pkl", "rb") as f:
                embed = pickle.load(f)
            await context.send(embed=embed)

        except Exception:
            print(traceback.format_exc())

    @commands.cooldown(1, 3600, commands.BucketType.user)
    @commands.hybrid_command(
        name="shimmer-richlist",
        description="Gives the Top 5 Shimmer addresses",
    )
    # This will only allow non-blacklisted members to execute the command
    @checks.not_blacklisted()
    async def shimmer_richlist(self, context: Context):
        """
        This command prints an embed with the top 5 Shimmer addresses.

        :param context: The application command context.
        """
        # Do your stuff here
        global bot_reply_channel_id
        # Do your stuff here

        if context.message.channel.id != int(bot_reply_channel_id):
            await context.send(
                f"This command can only be used in the <#{bot_reply_channel_id}> channel."
            )
            return

        try:
            with open("assets/embed_shimmer_richlist.pkl", "rb") as f:
                embed = pickle.load(f)
            await context.send(embed=embed)

        except Exception:
            print(traceback.format_exc())

    @commands.cooldown(1, 3600, commands.BucketType.user)
    @commands.hybrid_command(
        name="iota-distribution",
        description="Gives the IOTA token distribution",
    )
    # This will only allow non-blacklisted members to execute the command
    @checks.not_blacklisted()
    async def iota_distribution(self, context: Context):
        """
        This command prints a message with the IOTA token distribution.

        :param context: The application command context.
        """
        # Do your stuff here
        global bot_reply_channel_id
        # Do your stuff here

        if context.message.channel.id != int(bot_reply_channel_id):
            await context.send(
                f"This command can only be used in the <#{bot_reply_channel_id}> channel."
            )
            return

        try:
            with open("assets/message_iota_distribution.pkl", "rb") as f:
                message = pickle.load(f)
            await context.send(message)

        except Exception:
            print(traceback.format_exc())

    @commands.cooldown(1, 3600, commands.BucketType.user)
    @commands.hybrid_command(
        name="shimmer-distribution",
        description="Gives the Shimmer tokebn distribution",
    )
    # This will only allow non-blacklisted members to execute the command
    @checks.not_blacklisted()
    async def shimmer_distribution(self, context: Context):
        """
        This command prints a message with the Shimmer token distribution.

        :param context: The application command context.
        """
        # Do your stuff here
        global bot_reply_channel_id
        # Do your stuff here

        if context.message.channel.id != int(bot_reply_channel_id):
            await context.send(
                f"This command can only be used in the <#{bot_reply_channel_id}> channel."
            )
            return

        try:
            with open("assets/message_shimmer_distribution.pkl", "rb") as f:
                message = pickle.load(f)
            await context.send(message)

        except Exception:
            print(traceback.format_exc())


# And then we finally add the cog to the bot so that it can load, unload, reload and use it's content.
async def setup(bot):
    await bot.add_cog(Tokens(bot))
