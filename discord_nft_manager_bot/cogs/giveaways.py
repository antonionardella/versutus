# """"
# Copyright © antonionardella 2023 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
# Description:
# This is a template to create your own discord bot in python.

# Version: 5.4
# """

# import discord
# import pickle
# import traceback
# import hashlib

# import environ
# from discord.ext import commands
# from discord.ext.commands import Context
# from helpers import checks
# from helpers.ux_items import (
#     Button,
#     Confirm,
# )
# from helpers.logger import logger

# env = environ.Env()

# bot_reply_channel_id = env("BOT_REPLY_CHANNEL_ID")
# network = checks.network_type_variable()

# # Here we name the cog and create a new class for the cog.
# class Giveaways(commands.Cog, name="giveaways"):
#     def __init__(self, bot):
#         self.bot = bot

#     # Here you can just add your own commands, you'll always need to provide "self" as first parameter.

#     @commands.cooldown(1, 30, commands.BucketType.user)
#     @commands.hybrid_command(
#         name="shimmer-og-address-check",
#         description="Verify if you submitted a valid SMR address for the OG NFT airdrop",
#     )
#     # This will only allow non-blacklisted members to execute the command
#     @checks.not_blacklisted()
#     async def shimmer_og_address_check(self, context: Context):
#         """
#         This command compares the submitted address to the Shimmer OG NFT address list.

#         :param context: The application command context.
#         """
#         global bot_reply_channel_id
#         view = Confirm()
#         # Do your stuff here

#         if context.message.channel.id != int(bot_reply_channel_id):
#             await context.send(
#                 f"This command can only be used in the <#{bot_reply_channel_id}> channel."
#             )
#             return

#         try:

#             embed = discord.Embed(
#                 title=f"Would you like to verify Shimmer OG eligibility?",
#                 color=0x9C84EF,
#             )
#             embed.add_field(
#                 name="ℹ️ Requirements: ",
#                 value=f"Have the {network} address you submitted to the form ready.",
#                 inline=False,
#             )
#             await context.send(embed=embed, view=view, ephemeral=True)
#             await view.wait()

#             if view.value is None:
#                 logger.debug("Timed out...")
#                 await context.send("Timed out...")
#                 return

#             elif view.value:
#                 logger.debug("Confirmed modal")
#                 await context.send("Click to submit address", view=Button())

#             else:
#                 print("Cancelled...")
#                 logger.debug(
#                     f"/usersetup address input for {user_id} cancelled..."
#                 )
#                 await context.send(
#                     f"Address not added, call the `/shimmer add` command to add your {network} address.",
#                     ephemeral=True,
#                 )


#             # # Get's address and hashes it
#             # def hashInput(input_address):
#             #     a_string = str(input_address)
#             #     hashed_string = hashlib.sha256(a_string.encode('utf-8')).hexdigest()
#             #     return hashed_string

#             # with open('assets/encoded_og_addresses.csv', mode ='r', encoding='UTF8') as file:
#             #     for line in file.readlines():
#             #         if line.startswith(hashed_address)
#             # await context.send(embed=embed)

#             # # let's read the message
#             # if any(keyword in message.content.casefold() for keyword in keywords):
#             #     compare_hash_result = "ℹ️ Please insert a Shimmer address"
#             #     address_status = compare_hash_result
#             #     shimmer_reply_address = re.findall(shimmer_address_pattern, message.content, flags=re.IGNORECASE)
#             #     for shimmer_receiver_address in shimmer_reply_address:
#             #         compareHash(hashInput(shimmer_receiver_address))
#             #         address_status = compareHash(hashInput(shimmer_receiver_address))
#             #         if address_status == None:
#             #             address_status = "❌ Address NOT found!"

#             #     # as long as the sleep_switch is off
#             #     if self.sleep_switch == 0:
#             #     # Set the sleep_switch to 1 so that the bot only adds reactions instead of posting the embed
#             #         self.sleep_switch = 1

#             #         # build the embed message
#             #         embedVar=discord.Embed(title = "Shimmer OG NFT giveaway address check")
#             #         embedVar.add_field(name="Address Status", value=address_status, inline=True)

#             #         # reply to the input/command with the embed
#             #         await message.channel.send(embed=embedVar)

#             #         # define a thread for sleeping
#             #         sleep_thread = threading.Thread(target=self.thread_sleep)
#             #         # after posting the embed message go to sleep
#             #         sleep_thread.start()

#             #     # since the sleep_switch is at 1, the bot will only add the reaction to \
# a message and ignore further input/commands
#             #     else:
#             #         # react to the message
#             #         await message.add_reaction("😠")
#             # else:
#             #     logger.info("No informaion")

#         except Exception:
#             print(traceback.format_exc())

#     @commands.cooldown(1, 30, commands.BucketType.user)
#     @commands.hybrid_command(
#         name="",
#         description="Verify if you submitted a valid SMR address for the OG NFT airdrop",
#     )
#     # This will only allow non-blacklisted members to execute the command
#     @checks.not_blacklisted()
#     async def shimmer_og_address_check(self, context: Context):
#         """
#         This command compares the submitted address to the Shimmer OG NFT address list.

#         :param context: The application command context.
#         """
#         global bot_reply_channel_id
#         view = Confirm()
#         # Do your stuff here
# # And then we finally add the cog to the bot so that it can load, unload, reload and use it's content.
# async def setup(bot):
#     await bot.add_cog(Giveaways(bot))
