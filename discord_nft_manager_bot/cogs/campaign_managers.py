""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description: Campaign commands

Version: 5.4
"""

import discord
from discord import app_commands
from discord.ext import commands
from discord.ext.commands import Context
from helpers import checks, db_manager, ux_items


class CampaignManagers(commands.Cog, name="campaign managers"):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_group(name="nftcampaign", description="NFT campaign related data")
    @checks.not_blacklisted()
    async def nftcampaign(self, context: Context) -> None:
        """
        Lets you verify your information related to the NFT campaign.

        :param context: The hybrid command context.
        """
        if context.invoked_subcommand is None:
            embed = discord.Embed(
                title="NFT Campaign",
                description="You need to specify a subcommand.\n\n**Subcommands:**\n`add` - \
                    Add an address to your profile.\n`remove` - Remove an address from your profile.\n`list` - \
                        List the address saved in your profile.",
                color=0xE02B2B,
            )
            await context.send(embed=embed)

    @nftcampaign.command(
        base="nftcampaign",
        name="add",
        description="Lets you add a NFT campaign.",
    )
    @app_commands.describe(
        nft_campaign_name="The NFT campaign name.",
        role_name="The role which is eligible for the campaign",
        nft_campaign_nft_amount="The amount of NFTs available for this campaign",
    )
    @checks.not_blacklisted()
    # TODO insert check if user is campaign manager
    async def setup_nft_campaign(
        self,
        context: Context,
        nft_campaign_name: str,
        role_name: discord.Role,
        nft_campaign_nft_amount: int,
    ) -> None:
        """
        Will setup the NFT campaign.

        """
        view = ux_items.Confirm()
        nft_campaign_discord_role = discord.utils.get(
            context.guild.roles, name=str(role_name)
        )
        nft_campaign_discord_role_name = str(nft_campaign_discord_role.name)
        nft_campaign_discord_role_id = int(nft_campaign_discord_role.id)
        nft_campaign_manager_name = (
            str(context.author.name) + "#" + str(context.author.discriminator)
        )
        nft_campaign_manager_id = context.author.id
        nft_campaign_discord_role_eligible = True
        guild_id = context.guild.id

        embed = discord.Embed(title="Please confirm inserted data", color=0x9C84EF)
        embed.add_field(name="NFT campaign name: ", value=nft_campaign_name)
        embed.add_field(name="NFT amount: ", value=nft_campaign_nft_amount)
        embed.add_field(name="Eligible role: ", value=nft_campaign_discord_role_name)
        embed.add_field(name="NFT campaign manager: ", value=nft_campaign_manager_name)
        await context.send(embed=embed, view=view, ephemeral=True)
        await view.wait()

        if view.value is None:
            print("Timed out...")
            await context.send("Timed out...")

        elif view.value:
            print("Confirmed...")
            await db_manager.add_nft_campaign(
                guild_id,
                nft_campaign_name,
                nft_campaign_nft_amount,
                nft_campaign_manager_name,
                nft_campaign_manager_id,
                nft_campaign_discord_role_name,
                nft_campaign_discord_role_id,
                nft_campaign_discord_role_eligible,
            )
            embed = discord.Embed(title="Inserted data", color=0x9C84EF)
            embed.add_field(name="NFT ID: ", value=nft_campaign_name)
            embed.add_field(name="NFT amount: ", value=nft_campaign_nft_amount)
            embed.add_field(
                name="Eligible role: ", value=nft_campaign_discord_role_name
            )
            embed.add_field(
                name="NFT campaign manager: ", value=nft_campaign_manager_name
            )
            await context.send(embed=embed)

            # Add the campaign ID based on the campaign name
            # TODO this is an issue if a campaign has the same name as another one. Needs cleaning.
            nft_campaign_id = None
            nft_campaign = await db_manager.get_all_nft_campaign(guild_id)

            for campaign in nft_campaign:
                if campaign.nft_campaign_name == nft_campaign_name:
                    nft_campaign_id = campaign.id

            await db_manager.set_discord_role_as_eligible(
                guild_id, nft_campaign_discord_role_id, nft_campaign_id
            )
            # TODO Generate shimmer mainnet address
            # TODO Add Shimmer address to NFT campaign in DB
            # TODO make this an embed
            await context.send(
                f'"Thank you for confirming, data saved. \
                    Please now send {nft_campaign_nft_amount} NFTs to ADDRESS"'
            )
        else:
            print("Cancelled...")
            await context.send("Cancelled, call the /nftcampaign command again.")
        return

    # @nftcampaign.command(
    #     base="nftcampaign",
    #     name="test",
    #     description="Lets you test.",
    # )
    # @app_commands.describe()
    # @checks.not_blacklisted()
    # TODO insert check if user is campaign manager
    # async def setup_nft_campaign(self, context: Context) -> None:
    #     result = await db_manager.get_all_users()
    #     for discord_user in result:
    #         print(discord_user.user_name)


async def setup(bot):
    await bot.add_cog(CampaignManagers(bot))
