""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:

Version: 5.4
"""

import discord
from discord import app_commands, ui
from discord.ext import commands
from discord.ext.commands import Context
from helpers import checks, db_manager, network_based, nft_helper, ux_items
from helpers.logger import logger

network = checks.network_type_variable()


class Button(discord.ui.View):
    @discord.ui.button(
        label=f"Submit {network} address", style=discord.ButtonStyle.blurple
    )
    async def receive(
        self, interaction: discord.Interaction, button: discord.ui.Button
    ):
        # ephemeral=True makes the message hidden from everyone except the button presser
        # await interaction.response.send_message('Enjoy!', view=Counter(), ephemeral=True)
        await interaction.response.send_modal(AddressModal())


class AddressModal(ui.Modal, title=f"Submit your {network} address"):
    address = ui.TextInput(label=f"{network} address", min_length=63, max_length=63)

    async def on_submit(self, interaction: discord.Interaction):
        user_id = interaction.user.id
        guild_id = interaction.guild.id
        if await network_based.shimmer_address_format_check(str(self.address)):
            await network_based.shimmer_add_address_to_database(
                user_id, self.address, guild_id
            )
            await interaction.response.send_message(
                f"Thanks for your response.\n {network} {self.address} has been added!",
                ephemeral=True,
            )

        else:
            await interaction.response.send_message(
                f"Please check your {network} address, {self.address} is not valid!",
                ephemeral=True,
            )


class CampaignParticipants(commands.Cog, name="campaign participants"):
    def __init__(self, bot):
        self.bot = bot

    @commands.hybrid_command(name="usersetup", description="Setup your user")
    @checks.not_blacklisted()
    # @checks.user_not_set_up()
    async def update_user_roles(self, context: Context) -> None:
        """
        This code defines a Django model called DiscordUserSetup, which has fields for a foreign key to a DiscordUser
        and a DiscordGuild, as well as timestamps for when the setup was created and last updated, and a boolean field
        to indicate if the setup has been completed.

        It also defines two asynchronous functions, discord_user_setup_save and discord_user_setup_read, which are used
        to save and read details about a user's setup, respectively.

        The update_user_roles function is an asynchronous function that is used to update the roles of a user in a
        Discord guild. It starts by asking the user to confirm that they want to setup their user, and if the user
        confirms, it proceeds to save the user's information to the database using the db_manager.save_user function.
        It then deletes all of the user's roles from the database using the db_manager.clean_user_roles_to_database
        function and adds the user's current roles to the database using the db_manager.add_user_roles_to_database
        function. It then gets a list of all eligible roles on the server and all campaigns a user can claim, and shows
        them to the user.
        """

        user_id = context.author.id
        guild_id = context.guild.id
        user_name = context.author.name
        user_discriminator = context.author.discriminator

        view = ux_items.Confirm()
        view2 = ux_items.Confirm()

        embed = discord.Embed(
            title="Confirm that you want to setup your user to continue.",
            color=0x9C84EF,
        )
        embed.add_field(name="Privacy Policy ", value="https://privacy.policy")
        embed.add_field(name="Portal ", value="https://my.profile")

        user_setup = await db_manager.discord_user_setup_read(user_id, guild_id)
        if len(user_setup) > 0 and user_setup[0].setup_completed:
            # code to execute if setup is completed
            logger.debug(f"/usersetup for {user_id} is already completed")
            embed = discord.Embed(title="User setup", color=0x68CCCA)
            embed.add_field(
                name="Status: ",
                value="ℹ️ Your user is already set up.\n You can use `/profile` commands to edit your profile data.",
            )
            # embed.add_field(name = "Tip: ", value = "Use `/shimmer remove` to delete the old address.")
            await context.send(embed=embed, ephemeral=True)
        else:
            # code to execute if setup is not completed
            logger.debug("User setup is not completed.")
            await context.send(embed=embed, view=view, ephemeral=True)
            await view.wait()

            if view.value is None:
                logger.debug(f"/usersetup for {user_id} timed out...")
                await context.send("Timed out...")

            elif view.value:
                logger.debug(f"/usersetup for {user_id} confirmed...")

                # Save user info to DB
                await db_manager.save_user(user_id, user_name, user_discriminator)

                # Delete all roles from the user
                await db_manager.clean_user_roles_to_database(user_id, guild_id)

                # Add user roles to the DB
                user_roles = []
                for role in context.author.roles:
                    if role.name != "@everyone":
                        user_roles.append(role.id)
                        role_id = role.id
                        await db_manager.add_user_roles_to_database(
                            user_id, role_id, guild_id
                        )

                user_eligible_roles_embed_joined = (
                    await nft_helper.get_eligible_campaign_role_name_for_user(
                        guild_id, user_id
                    )
                )

                embed = discord.Embed(title="Inserted data", color=0x00FF00)
                embed.add_field(
                    name="User information", value="✅ Saved correctly", inline=False
                )
                embed.add_field(
                    name="NFT eligible roles: ",
                    value=user_eligible_roles_embed_joined,
                    inline=False,
                )
                # embed.add_field(name = "Top role:", value=context.author.top_role)

                # Save setup complete info to DB
                await db_manager.discord_user_setup_save(user_id, guild_id)
                await context.send(embed=embed, ephemeral=True)

                # Check if user has address and offer to add it now
                if not await network_based.shimmer_address_in_db_check(
                    user_id, guild_id
                ):
                    embed = discord.Embed(
                        title=f"Would you like to add your {network} address now?",
                        color=0x9C84EF,
                    )
                    embed.add_field(
                        name="Privacy Policy ", value="https://privacy.policy"
                    )
                    embed.add_field(name="Portal ", value="https://my.profile")
                    embed.add_field(
                        name="ℹ️ Requirements: ",
                        value=f"Open your {network} wallet and have your address ready.",
                        inline=False,
                    )
                    await context.send(embed=embed, view=view2, ephemeral=True)
                    await view2.wait()

                    if view2.value is None:
                        logger.debug("Timed out...")
                        await context.send("Timed out...")
                        return

                    elif view2.value:
                        logger.debug("Confirmed modal")
                        await context.send("Click to submit address", view=Button())

                    else:
                        print("Cancelled...")
                        logger.debug(
                            f"/usersetup address input for {user_id} cancelled..."
                        )
                        await context.send(
                            f"Address not added, call the `/shimmer add` command to add your {network} address.",
                            ephemeral=True,
                        )

                else:
                    embed = discord.Embed(
                        title=f"Submitted {network} address", color=0x68CCCA
                    )
                    embed.add_field(
                        name="Status: ",
                        value=f"ℹ️ The user already has a {network} address.",
                        inline=False,
                    )
                    embed.add_field(
                        name="Tip: ",
                        value=f"Use `/shimmer remove` to delete the old {network} address.",
                        inline=False,
                    )
                    await context.send(embed=embed, ephemeral=True)
                    return

            else:
                print("Cancelled...")
                logger.debug(f"/usersetup for {user_id} cancelled...")
                await context.send(
                    "Cancelled, call the `/usersetup` command to restart."
                )
            return

    @commands.hybrid_command(
        name="claim_all_nfts", description="Claim NFTs from all campaigns"
    )
    @checks.not_blacklisted()
    async def claim_all_nfts(self, context: Context) -> None:
        """ """

        user_id = context.author.id
        guild_id = context.guild.id
        # user_name = context.author.name
        # user_discriminator = context.author.discriminator
        user_setup = await db_manager.discord_user_setup_read(user_id, guild_id)

        if not await network_based.shimmer_address_in_db_check(user_id, guild_id):
            # code to execute if setup is not completed
            logger.debug(f"User {user_id} has no address.")
            embed = discord.Embed(title="⚠️ Warning", color=0x64B5F6)
            embed.add_field(
                name="Address status:  ",
                value=f"{network} address is missing in your profile",
                inline=False,
            )
            embed.add_field(
                name="ℹ️ Requirements: ",
                value=f"Open your {network} wallet and have your adrress ready. \
                    Then use `/shimmer add` to add your address.",
                inline=False,
            )
            await context.send(embed=embed, ephemeral=True)
            return

        if not user_setup[0].setup_completed:
            # code to execute if setup is not completed
            logger.debug(f"User setup for {user_id} is not completed.")
            # TODO ASK IF USER WANTS TO SET UP NOW AND CALL ROUTINE HERE
            embed = discord.Embed(title="⚠️ Warning", color=0x64B5F6)
            embed.add_field(
                name="Setup status:  ", value="User setup not completed", inline=False
            )
            embed.add_field(
                name="ℹ️ Requirements: ",
                value="Use `/usersetup` to complete the setup first.",
                inline=False,
            )
            await context.send(embed=embed, ephemeral=True)
            return

        # code to execute if setup is completed
        logger.debug(
            f"address and usersetup for {user_id} is complete we can continue to claim"
        )
        view = ux_items.Confirm()
        embed = discord.Embed(
            title="Confirm that you want to claim eligible NFTs from all campaigns to continue.",
            color=0x9C84EF,
        )
        embed.add_field(name="Privacy Policy ", value="https://privacy.policy")
        embed.add_field(name="Portal ", value="https://my.profile")

        await context.send(embed=embed, view=view, ephemeral=True)
        await view.wait()

        if view.value is None:
            logger.debug(f"/claim_all_nfts for {user_id} timed out...")
            await context.send("Timed out...")
            return

        elif view.value:
            print("Confirmed...")
            logger.debug(f"/claim_all_nfts for {user_id} confirmed...")

            user_eligible_role_ids = await nft_helper.get_eligible_campaigns_for_user(
                guild_id, user_id
            )

            embed = discord.Embed(title="Inserted data", color=0x00FF00)
            embed.add_field(
                name="User information", value="✅ Saved correctly", inline=False
            )
            embed.add_field(
                name="NFT eligible roles: ", value=user_eligible_role_ids, inline=False
            )
            # embed.add_field(name = "Top role:", value=context.author.top_role)
            await context.send(embed=embed, ephemeral=True)
            return

        else:
            print("Cancelled...")
            logger.debug(f"/claim_all_nfts for {user_id} cancelled...")
            await context.send(
                "Cancelled, call the `/claim_all_nfts` command to restart."
            )
            return

    @commands.hybrid_group(name="profile", description="profile related data")
    @checks.not_blacklisted()
    async def profile(self, context: Context) -> None:
        """
        Lets you verify your information related to Shimmer.

        :param context: The hybrid command context.
        """
        if context.invoked_subcommand is None:
            embed = discord.Embed(
                title="Profile",
                description="You need to specify a subcommand.\n\n**Subcommands:**\n`add` - Add an address to your \
                     profile.\n`remove` - Remove an address from your profile.\n`list` - List the address saved in your\
                         profile.",
                color=0xE02B2B,
            )
            await context.send(embed=embed)

    @profile.command(
        base="profile",
        name="add-address",
        description="Lets you add a Shimmer address to your profile.",
    )
    @app_commands.describe(address="The Shimmer address to be added to your profile")
    @checks.not_blacklisted()
    async def shimmer_address_add(self, context: Context, *, address: str) -> None:
        """
        Lets you add an address to your profile.

        :param context: The hybrid command context.
        :param user: The Shimmer address that should be added to your profile.
        """
        user_id = context.author.id
        guild_id = context.guild.id

        if await network_based.shimmer_address_in_db_check(user_id, guild_id):
            embed = discord.Embed(title=f"Submitted {network} address", color=0x68CCCA)
            embed.add_field(
                name="Status: ",
                value=f"ℹ️ The user already has a {network} address.",
                inline=False,
            )
            embed.add_field(
                name="Tip: ",
                value=f"Use `/shimmer remove` to delete the old {network} address.",
                inline=False,
            )
            await context.send(embed=embed, ephemeral=True)
            return

        if await network_based.shimmer_address_format_check(address):
            await network_based.shimmer_add_address_to_database(
                user_id, address, guild_id
            )
            embed = discord.Embed(title=f"Submitted {network} address", color=0x00FF00)
            embed.add_field(
                name="Status: ",
                value=f"✅ {network} address saved succesfully",
                inline=False,
            )
            embed.add_field(name="Address submitted: ", value=address, inline=False)
            await context.send(embed=embed, ephemeral=True)
            return
        else:
            embed = discord.Embed(title="Submitted {network} address", color=0xFF0000)
            embed.add_field(
                name="Status: ",
                value=f"❌ The data inserted is not a valid {network} address",
                inline=False,
            )
            embed.add_field(name="Address submitted: ", value=address, inline=False)
            await context.send(embed=embed, ephemeral=True)
            return

    @profile.command(
        base="profile",
        name="remove-address",
        description="Lets you remove the Shimmer address from your profile.",
    )
    @checks.not_blacklisted()
    async def shimmer_address_remove(self, context: Context) -> None:
        """
        Will remove the address from your profile.

        """
        user_id = context.author.id
        guild_id = context.guild.id

        if await network_based.shimmer_address_in_db_check(user_id, guild_id):
            await network_based.shimmer_remove_address_from_database(user_id, guild_id)
            embed = discord.Embed(title=f"Submitted {network} address", color=0x68CCCA)
            embed.add_field(
                name="Status: ",
                value=f"ℹ️ The Shimmer {network} address has been removed.",
                inline=False,
            )
            await context.send(embed=embed, ephemeral=True)
            return
        else:
            embed = discord.Embed(title="Error", color=0xFF0000)
            embed.add_field(
                name="Status: ",
                value=f"❌ There is no {network} address saved for this user",
                inline=False,
            )
            await context.send(embed=embed, ephemeral=True)
            return


async def setup(bot):
    await bot.add_cog(CampaignParticipants(bot))
