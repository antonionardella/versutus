""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:
This is a series of functions to get lits of data from the database.

Version: 5.4
"""

import os
import sys

# Django access
import django
from helpers import db_manager
from helpers.logger import logger

sys.path.append("..")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "DiscordOauth2.settings")
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()


async def get_list_of_nft_campaigns(guild_id):
    nft_campaign = []
    nft_campaign_object = await db_manager.get_all_nft_campaign(guild_id)

    for campaign in nft_campaign_object:
        nft_campaign.append(campaign.nft_campaign_discord_role_id)
        return nft_campaign


async def get_eligible_campaigns_for_user(guild_id):
    eligible_roles = []
    eligible_roles_object = await db_manager.get_all_eligible_roles(guild_id)
    for role in eligible_roles_object:
        eligible_roles.append(role.role_id)
        logger.debug("eligible roles")
        logger.debug(eligible_roles)

    nft_campaign = []
    nft_campaign_object = await db_manager.get_all_nft_campaign(guild_id)
    logger.debug("nft_campaign_object")
    logger.debug(nft_campaign_object)

    for campaign in nft_campaign_object:
        campaign_roles = await db_manager.get_all_guild_roles_by_id(
            campaign.nft_campaign_discord_role_id
        )
        logger.debug("campaign_roles")
        logger.debug(campaign_roles)
        if campaign_roles:
            for role in campaign_roles:
                nft_campaign.append(role.role_id)
                logger.debug("nft campaign")
                logger.debug(nft_campaign)

    nft_campaign = set(nft_campaign)
    eligible_roles = set(eligible_roles)
    user_eligible_roles = list(set(nft_campaign).intersection(set(eligible_roles)))
    return user_eligible_roles


async def get_eligible_campaign_role_name_for_user(guild_id, user_id):
    user_eligible_roles = await get_eligible_campaigns_for_user(guild_id, user_id)
    print("user_eligible_roles2")
    print(user_eligible_roles)

    user_eligible_roles_embed = []
    for role_id in user_eligible_roles:
        roles = await db_manager.get_all_guild_roles_by_id(role_id)
        if roles:
            role = roles[0]
            user_eligible_roles_embed.append(role.role_name)

    logger.debug("user eligible")
    logger.debug(user_eligible_roles)

    logger.debug("user_eligible_roles_embed")
    logger.debug(user_eligible_roles_embed)

    user_eligible_roles_embed_joined = ", ".join(user_eligible_roles_embed)
    return user_eligible_roles_embed_joined
