""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:
This file contains functions that work differently based on the debug_mode status in the config.json file

Version: 5.4
"""

from collections.abc import Callable

from helpers import checks, db_manager


def shimmer_address_in_db_check(user_id, guild_id) -> Callable[[int, int], bool | str]:
    """
    This function verifies if the user already has an address in the database for the devnet or the mainnet
    """
    if checks.debug_server():
        return checks.checkUserHasShimmerDevnetAddress(user_id, guild_id)
    else:
        return checks.checkUserHasShimmerMainnetAddress(user_id, guild_id)


def shimmer_address_format_check(address) -> Callable[[str], bool]:
    """
    This function verifies if the address format is correct for the devnet or the mainnet
    """
    if checks.debug_server():
        return checks.checkShimmerDevnetAddressFormat(address)
    else:
        return checks.checkShimmerMainnetAddressFormat(address)


async def shimmer_add_address_to_database(
    user_id, address, guild_id
) -> Callable[[str], bool]:
    """
    This function adds the address for the devnet or the mainnet
    """
    if checks.debug_server():
        await db_manager.add_user_shimmer_devnet_address_to_database(
            user_id, address, guild_id
        )
    else:
        await db_manager.add_user_shimmer_mainnet_address_to_database(
            user_id, address, guild_id
        )


async def shimmer_remove_address_from_database(
    user_id, guild_id
) -> Callable[[int, int], bool]:
    """
    This function adds the address for the devnet or the mainnet
    """
    if checks.debug_server():
        await db_manager.remove_user_shimmer_devnet_address_from_database(
            user_id, guild_id
        )
    else:
        await db_manager.remove_user_shimmer_mainnet_address_from_database(
            user_id, guild_id
        )
