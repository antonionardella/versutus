""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:
This file contains functions for the Shimmer network

Version: 5.4
"""

import pickle

import discord


async def create_empty_embed_and_messages():
    embed = discord.Embed(title="🫰 Shimmer Top 5 Richlist", color=0x00FF00)

    embed.add_field(name="Updates: ", value="Every 24h")
    embed.add_field(
        name="❌ Richlist not available yet: ",
        value="Please have patience, the richlist will be available soon.",
    )
    with open("assets/embed_shimmer_richlist.pkl", "wb") as f:
        pickle.dump(embed, f)

    embed = discord.Embed(title="🫰 IOTA Top 5 Richlist", color=0x00FF00)
    embed.add_field(name="Updates: ", value="Every 24h")
    embed.add_field(
        name="❌ Richlist not available yet: ",
        value="Please have patience, the richlist will be available soon.",
    )
    with open("assets/embed_iota_richlist.pkl", "wb") as f:
        pickle.dump(embed, f)

    msg = "**IOTA token distribution**\n\n"
    msg += "Updates: Every 24h\n\n"
    msg += "❌ Distribution not available yet: Please have patience, the table will be available soon."
    with open("assets/message_iota_distribution.pkl", "wb") as f:
        pickle.dump(msg, f)

    msg = "**Shimmer token distribution**\n\n"
    msg += "Updates: Every 24h\n\n"
    msg += "❌ Distribution not available yet: Please have patience, the table will be available soon."
    with open("assets/message_shimmer_distribution.pkl", "wb") as f:
        pickle.dump(msg, f)
