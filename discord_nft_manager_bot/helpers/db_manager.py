""""
Copyright © Krypton 2022 - https://github.com/kkrypt0nn (https://krypton.ninja)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""
import os
import sys

# Django access
import django
from helpers.logger import logger

sys.path.append("..")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "versutus.settings")
# sys.path.append(str(BASE_DIR / "versutus"))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.local")
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"
django.setup()
from versutus.tangle.models import (  # noqa: E402
    BlacklistedUser,
    DiscordGuild,
    DiscordGuildRoles,
    DiscordUser,
    DiscordUserRoles,
    DiscordUserSetup,
    DiscordUserShimmerAddresses,
    EcosystemOpportunities,
    IotaDistribution,
    IotaHexAddress,
    IotaTopAddress,
    NftCampaign,
    NftEligibleDiscordGuildRoles,
    ShimmerDistribution,
    ShimmerHexAddress,
    ShimmerTopAddress,
)


async def add_guild_id_to_db(guild_id: int):
    """
    This function will populate the guild's (Discord server) information in the database.

    """
    add_guild, created = DiscordGuild.objects.update_or_create(guild_id=guild_id)
    add_guild.save()


async def add_guild_roles_to_database(
    role_id: int, role_name: str, guild_id: int
) -> int:
    """
    This function will add all guild roles to the database.
    """
    # Check if the guild_id exists
    logger.debug("Check if the guild_id exists")
    logger.debug(guild_id)
    guild_from_database = DiscordGuild.objects.filter(guild_id=guild_id).first()
    logger.debug(guild_from_database)

    if guild_from_database is not None:
        add_guild_roles, created = DiscordGuildRoles.objects.update_or_create(
            role_id=role_id,
            role_name=str(role_name),
            guild_id=guild_from_database.guild_id,
        )
        add_guild_roles.save()


async def add_user_roles_to_database(user_id: int, role_id: int, guild_id: int):
    user = DiscordUser.objects.get(user_id=user_id)
    role = DiscordGuildRoles.objects.get(role_id=role_id)
    guild = DiscordGuild.objects.get(guild_id=guild_id)
    add_user_roles, created = DiscordUserRoles.objects.update_or_create(
        user=user, role=role, guild=guild
    )
    add_user_roles.save()


async def clean_user_roles_to_database(user_id: int, guild_id: int):
    """
    This function will delete all of the user role information for the specific guild in the database.

    """
    DiscordUserRoles.objects.filter(
        guild=str(guild_id), user=user_id
    ).delete()  # Delete from Database using ORM


async def remove_user_shimmer_mainnet_address_from_database(
    user_id: int, guild_id: int
) -> int:
    """
    This function will remove shimmer mainnet address of a user from the database.

    """
    DiscordUserShimmerAddresses.objects.filter(
        guild=str(guild_id), user=user_id
    ).update(shimmer_mainnet_address="")


async def remove_user_shimmer_devnet_address_from_database(
    user_id: int, guild_id: int
) -> int:
    """
    This function will remove shimmer devnet address of a user from the database.

    """
    DiscordUserShimmerAddresses.objects.filter(
        guild=str(guild_id), user=user_id
    ).update(shimmer_devnet_address="")


async def get_all_users():
    return list(DiscordUser.objects.all())


async def get_all_user_roles(user_id: int):
    return list(DiscordUserRoles.objects.filter(user=user_id))


async def get_all_guild_roles_by_id(role_id: int):
    return list(DiscordGuildRoles.objects.filter(role_id=role_id))


async def save_user(user_id: int, user_name, user_discriminator):
    """
    This function will populate the user's information in the database.

    """
    save_user, created = DiscordUser.objects.update_or_create(
        user_id=user_id, user_name=str(user_name) + "#" + str(user_discriminator)
    )  # Save to Database using ORM
    save_user.save()


async def discord_user_setup_save(user_id: int, guild_id: int):
    """
    This function save details if the Discord user went through the setup, in the database.

    """
    # Get the user and guild objects from the database
    user = DiscordUser.objects.get(user_id=user_id)
    guild = DiscordGuild.objects.get(guild_id=guild_id)
    # Create or update the DiscordUserSetup object
    user_setup, created = DiscordUserSetup.objects.update_or_create(
        user=user, guild=guild, defaults={"setup_completed": True}
    )
    user_setup.save()


async def discord_user_setup_read(user_id: int, guild_id: int):
    """
    This function read details if the Discord user went through the setup, in the database.

    """
    return list(
        DiscordUserSetup.objects.select_related().filter(user=user_id, guild=guild_id)
    )


async def add_user_shimmer_mainnet_address_to_database(
    user_id: int, address: str, guild_id: int
) -> int:
    """
    This function will add Shimmer mainnet address roles of a user to the database.
    """
    # Get the user and guild objects from the database
    user = DiscordUser.objects.get(user_id=user_id)
    guild = DiscordGuild.objects.get(guild_id=guild_id)

    add_mainnet_address, created = DiscordUserShimmerAddresses.objects.get_or_create(
        user=user, guild=guild, defaults={"shimmer_mainnet_address": address}
    )
    if not created:
        add_mainnet_address.shimmer_mainnet_address = address
        add_mainnet_address.save()


async def add_user_shimmer_devnet_address_to_database(
    user_id: int, address: str, guild_id: int
) -> int:
    """
    This function will add Shimmer devnet address roles of a user to the database.
    """
    # Get the user and guild objects from the database
    user = DiscordUser.objects.get(user_id=user_id)
    guild = DiscordGuild.objects.get(guild_id=guild_id)

    add_devnet_address, created = DiscordUserShimmerAddresses.objects.get_or_create(
        user=user, guild=guild, defaults={"shimmer_devnet_address": address}
    )
    if not created:
        add_devnet_address.shimmer_devnet_address = address
        add_devnet_address.save()


async def user_has_dlt_address(user_id: int, guild_id: int):
    """
    This function reads if the Discord user has a Shimmer address, in the database.

    """
    return DiscordUserShimmerAddresses.objects.filter(user=user_id, guild=guild_id)


async def get_all_eligible_roles(guild_id: int):
    return list(
        NftEligibleDiscordGuildRoles.objects.select_related().filter(guild=guild_id)
    )


async def add_nft_campaign(
    guild_id,
    nft_campaign_name,
    nft_campaign_nft_amount,
    nft_campaign_manager_name,
    nft_campaign_manager_id,
    nft_campaign_discord_role,
    nft_campaign_discord_role_id,
    nft_campaign_discord_role_eligible,
):
    """
    This function adds an NFT campaign in the database
    """
    # Get the guild objects from the database
    guild = DiscordGuild.objects.get(guild_id=guild_id)

    add_campaign, created = NftCampaign.objects.update_or_create(
        guild=guild,
        nft_campaign_name=nft_campaign_name,
        nft_campaign_nft_amount=nft_campaign_nft_amount,
        nft_campaign_manager_name=nft_campaign_manager_name,
        nft_campaign_manager_id=nft_campaign_manager_id,
        nft_campaign_discord_role=nft_campaign_discord_role,
        nft_campaign_discord_role_id=nft_campaign_discord_role_id,
        nft_campaign_discord_role_eligible=nft_campaign_discord_role_eligible,
    )  # Save to Database using ORM
    add_campaign.save()


async def get_all_nft_campaign(guild_id: int):
    return list(NftCampaign.objects.filter(guild=guild_id))


async def set_discord_role_as_eligible(
    guild_id: int, nft_campaign_discord_role_id, nft_campaign_id
):
    """
    This function will describe if the Discord user went through the setup, in the database.

    """
    guild = DiscordGuild.objects.get(guild_id=guild_id)
    role = DiscordGuildRoles.objects.get(role_id=nft_campaign_discord_role_id)

    set_role, created = NftEligibleDiscordGuildRoles.objects.update_or_create(
        guild=guild, role=role, campaign_id=nft_campaign_id
    )  # Save to Database using ORM
    set_role.save()


async def is_blacklisted(user_id: int) -> bool:
    """
    This function will check if a user is blacklisted.

    :param user_id: The ID of the user that should be checked.
    :return: True if the user is blacklisted, False if not.
    """
    blacklisted_user = BlacklistedUser.objects.filter(user_id=user_id).first()
    return blacklisted_user is not None


# async def add_user_to_blacklist(user_id: int) -> int:
#     """
#     This function will add a user based on its ID in the blacklist.

#     :param user_id: The ID of the user that should be added into the blacklist.
#     """
#     try:
#         await asyncio.get_event_loop().run_in_executor(None, BlacklistedUser.objects.create, user_id=user_id)
#     except IntegrityError:
#         # User is already blacklisted
#         pass
#     return await asyncio.get_event_loop().run_in_executor(None, BlacklistedUser.objects.count)

# async def remove_user_from_blacklist(user_id: int) -> int:
#     """
#     This function will remove a user based on its ID from the blacklist.

#     :param user_id: The ID of the user that should be removed from the blacklist.
#     """
#     async with aiosqlite.connect("../database/discord-bot_database.db") as db:
#         await db.execute("DELETE FROM blacklist WHERE user_id=?", (user_id,))
#         await db.commit()
#         rows = await db.execute("SELECT COUNT(*) FROM blacklist")
#         async with rows as cursor:
#             result = await cursor.fetchone()
#             return result[0] if result is not None else 0

# async def add_warn(user_id: int, server_id: int, moderator_id: int, reason: str) -> int:
#     """
#     This function will add a warn to the database.

#     :param user_id: The ID of the user that should be warned.
#     :param reason: The reason why the user should be warned.
#     """
#     async with aiosqlite.connect("../database/discord-bot_database.db") as db:
#         rows = await db.execute("SELECT id FROM warns WHERE user_id=? AND server_id=? ORDER BY id DESC LIMIT 1", \
# (user_id, server_id,))
#         async with rows as cursor:
#             result = await cursor.fetchone()
#             warn_id = result[0] + 1 if result is not None else 1
#             await db.execute("INSERT INTO warns(id, user_id, server_id, moderator_id, reason) \
#               VALUES (?, ?, ?, ?, ?)", (warn_id, user_id, server_id, moderator_id, reason,))
#             await db.commit()
#             return warn_id


# async def remove_warn(warn_id: int, user_id: int, server_id: int) -> int:
#     """
#     This function will remove a warn from the database.

#     :param warn_id: The ID of the warn.
#     :param user_id: The ID of the user that was warned.
#     :param server_id: The ID of the server where the user has been warned
#     """
#     async with aiosqlite.connect("../database/discord-bot_database.db") as db:
#         await db.execute("DELETE FROM warns WHERE id=? AND user_id=? AND server_id=?", (warn_id, user_id, server_id,))
#         await db.commit()
#         rows = await db.execute("SELECT COUNT(*) FROM warns WHERE user_id=? AND server_id=?", (user_id, server_id,))
#         async with rows as cursor:
#             result = await cursor.fetchone()
#             return result[0] if result is not None else 0


# async def get_warnings(user_id: int, server_id: int) -> list:
#     """
#     This function will get all the warnings of a user.

#     :param user_id: The ID of the user that should be checked.
#     :param server_id: The ID of the server that should be checked.
#     :return: A list of all the warnings of the user.
#     """
#     async with aiosqlite.connect("../database/discord-bot_database.db") as db:
#         rows = await db.execute("SELECT user_id, server_id, moderator_id, reason, strftime('%s', created_at), \
#           id FROM warns WHERE user_id=? AND server_id=?", (user_id, server_id,))
#         async with rows as cursor:
#             result = await cursor.fetchall()
#             result_list = []
#             for row in result:
#                 result_list.append(row)
#             return result_list


async def clean_guild_roles_from_database(guild_id: str) -> int:
    """
    This function will clean a all guild roles from the database.
    """
    # Get the roles that belong to the specified guild
    roles = DiscordGuildRoles.objects.filter(guild=guild_id)

    # Delete the roles
    roles.delete()
    logger.debug("All roles deleted")


############################################
# TOKEN DATA QUERIES                       #
############################################
# IOTA
############################################


async def add_iota_ledger(data):  # , table_name: str):
    """
    This function will write the Ledger state to the db.

    :param user_id: address and balance in the ledger.
    :param table_name: The name of the table where the data is stored.
    """
    # First we clean the content
    iota_ledger = IotaHexAddress.objects.all()

    # Delete the IOTA ledger
    iota_ledger.delete()
    logger.debug("IOTA Ledger cleaned")

    # Now we update the ledger
    data_list = [
        (address["address"], address["balance"])
        for address in data["data"]["addresses"]
    ]
    iota_addresses = [
        IotaHexAddress(address=address[0], balance=address[1]) for address in data_list
    ]
    IotaHexAddress.objects.bulk_create(iota_addresses)


async def get_iota_ledger():  # table_name: str):
    """
    This function will get the Ledger state from the db.

    :param table_name: The name of the table where the data is stored.
    """
    return list(IotaHexAddress.objects.all())


async def add_iota_top_addresses(data):  # , table_name):
    """
    This function will write the top IOTA addreses state to the db.

    :param table_name: The name of the table where the data is stored.
    """
    # First we clean the content
    iota_top_addresses = IotaTopAddress.objects.all()

    # Delete the IOTA ledger
    iota_top_addresses.delete()
    logger.debug("IOTA Top Addresses cleaned")

    # Now we update the top addresses
    data_list = data
    iota_addresses = [
        IotaTopAddress(address=address[0], balance=address[1]) for address in data_list
    ]
    IotaTopAddress.objects.bulk_create(iota_addresses)
    logger.debug("IOTA Top Addresses saved")


async def get_iota_top_addresses():  # table_name: str):
    """
    This function will get the Top addresses in bech32 format from the db.

    :param table_name: The name of the table where the data is stored.
    """
    return list(IotaTopAddress.objects.all())


async def save_iota_distribution(summary_table):
    """
    This function will get save the IOTA token distribution to the DB.

    :param summary_table: Distribution table generated by the function.
    """
    # First we clean the content
    iota_distribution = IotaDistribution.objects.all()

    # Delete the IOTA ledger
    iota_distribution.delete()

    logger.debug("IOTA Distribution cleaned")
    print(iota_distribution)

    for index, row in summary_table.iterrows():
        iota_distribution = IotaDistribution(
            amounts=row["Amounts"],
            addresses=row.Addresses,
            sum_balances=row["Sum balances"],
            percent_addresses=row["% Addresses"],
            percent_supply=row["% Supply"],
        )
        iota_distribution.save()


# Shimmer
############################################


async def clean_shimmer_ledger():
    """
    This function will clean the Ledger state in the db.

    :param user_id: address and balance in the ledger.
    :param table_name: The name of the table where the data is stored.
    """

    shimmer_ledger = ShimmerHexAddress.objects.all()
    # Delete the Shimmer ledger
    shimmer_ledger.delete()
    logger.debug("Shimmer Ledger cleaned")


async def add_shimmer_ledger(data):
    """
    This function will write the Ledger state to the db.

    :param user_id: address and balance in the ledger.
    :param table_name: The name of the table where the data is stored.
    """
    # Now we update the ledger
    data_list = [(address, balance) for address, balance in data.items()]
    # data_list = [(address["address"], address["balance"]) for address in data['data']['addresses']]
    shimmer_addresses = [
        ShimmerHexAddress(address=address[0], balance=address[1])
        for address in data_list
    ]
    ShimmerHexAddress.objects.bulk_create(shimmer_addresses)


async def get_shimmer_ledger():
    """
    This function will get the Ledger state from the db.

    :param table_name: The name of the table where the data is stored.
    """
    return list(ShimmerHexAddress.objects.all())


async def add_shimmer_top_addresses(data):
    """
    This function will write the top Shimmer addreses state to the db.

    :param table_name: The name of the table where the data is stored.
    """
    # First we clean the content
    shimmer_top_addresses = ShimmerTopAddress.objects.all()

    # Delete the Shimmer ledger
    shimmer_top_addresses.delete()
    logger.debug("Shimmer Top Addresses cleaned")

    # Now we update the top addresses
    data_list = data
    shimmer_addresses = [
        ShimmerTopAddress(address=address[0], balance=address[1])
        for address in data_list
    ]
    ShimmerTopAddress.objects.bulk_create(shimmer_addresses)
    logger.debug("Shimmer Top Addresses saved")


async def get_shimmer_top_addresses():
    """
    This function will get the Top addresses in bech32 format from the db.

    :param table_name: The name of the table where the data is stored.
    """
    return list(ShimmerTopAddress.objects.all())


async def save_shimmer_distribution(summary_table):
    """
    This function will get save the Shimmer token distribution to the DB.

    :param summary_table: Distribution table generated by the function.
    """
    # First we clean the content
    shimmer_distribution = ShimmerDistribution.objects.all()

    # Delete the Shimmer ledger
    shimmer_distribution.delete()

    logger.debug("Shimmer Distribution cleaned")
    # print(shimmer_distribution)

    for index, row in summary_table.iterrows():
        shimmer_distribution = ShimmerDistribution(
            amounts=row["Amounts"],
            addresses=row.Addresses,
            sum_balances=row["Sum balances"],
            percent_addresses=row["% Addresses"],
            percent_supply=row["% Supply"],
        )
        shimmer_distribution.save()


async def add_opportunities(opportunities):
    """
    This function will write the opportunities to the db.

    :param opportunities: The threads extracted from the Discord forum.
    """
    # First we remove archived Opportunities
    EcosystemOpportunities.objects.filter(archived=True).delete()
    logger.debug("Archived opportunities removed")

    # Create EcosystemOpportunities objects for each opportunity
    objects = [EcosystemOpportunities(**opportunity) for opportunity in opportunities]

    # Now we bulk add the non-archived opportunities
    EcosystemOpportunities.objects.bulk_create(objects)
    logger.debug("Opportunities added")


async def get_opportunity_by_thread_id(thread_id):
    """
    This function will get the opportunities from the db.
    """
    return EcosystemOpportunities.objects.filter(thread_id=thread_id).first()


async def get_all_opportunities():
    """
    This function will get all opportunities from the db.
    """
    return EcosystemOpportunities.objects.all()


async def remove_opportunities_by_thread_ids(thread_ids):
    """
    This function will remove opportunities from the db by thread IDs.
    """
    EcosystemOpportunities.objects.filter(thread_id__in=thread_ids).delete()
    logger.debug(f"Removed opportunities with thread IDs: {thread_ids}")
