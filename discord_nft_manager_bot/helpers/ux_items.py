""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linfree.antonionardella.it)
Description:
UX helpers for the Discord bot

Version: 5.4
"""

import discord
from helpers import checks, network_based

network = checks.network_type_variable()


# Define a simple View that gives us a confirmation menu
class Confirm(discord.ui.View):
    def __init__(self):
        super().__init__()
        self.value = None

    # When the confirm button is pressed, set the inner value to `True` and
    # stop the View from listening to more input.
    # We also send the user an ephemeral message that we're confirming their choice.
    @discord.ui.button(label="Confirm", style=discord.ButtonStyle.green)
    async def confirm(
        self, interaction: discord.Interaction, button: discord.ui.Button
    ):
        await interaction.response.send_message("Confirming", ephemeral=True)
        self.value = True
        self.stop()

    # This one is similar to the confirmation button except sets the inner value to `False`
    @discord.ui.button(label="Cancel", style=discord.ButtonStyle.grey)
    async def cancel(self, interaction: discord.Interaction, button: discord.ui.Button):
        await interaction.response.send_message("Cancelling", ephemeral=True)
        self.value = False
        self.stop()


class Button(discord.ui.View):
    @discord.ui.button(
        label=f"Submit {network} address", style=discord.ButtonStyle.blurple
    )
    async def receive(
        self, interaction: discord.Interaction, button: discord.ui.Button
    ):
        # ephemeral=True makes the message hidden from everyone except the button presser
        # await interaction.response.send_message('Enjoy!', view=Counter(), ephemeral=True)
        await interaction.response.send_modal(AddressModal())


class AddressModal(discord.ui.Modal, title=f"Submit your {network} address"):
    address = discord.ui.TextInput(
        label=f"{network} address", min_length=63, max_length=63
    )

    async def on_submit(self, interaction: discord.Interaction):
        user_id = interaction.user.id
        guild_id = interaction.guild.id
        if await network_based.shimmer_address_format_check(str(self.address)):
            await network_based.shimmer_add_address_to_database(
                user_id, self.address, guild_id
            )
            await interaction.response.send_message(
                f"Thanks for your response.\n {network} {self.address} has been added!",
                ephemeral=True,
            )

        else:
            await interaction.response.send_message(
                f"Please check your {network} address, {self.address} is not valid!",
                ephemeral=True,
            )
