""""
Copyright © antonionardella 2022 - https://github.com/antonionardella (https://linkfree.antonionardella.it)
Description:
This file contains functions for the Shimmer network

Version: 5.4
"""
import os

import environ
import iota_wallet
from helpers.logger import logger
from iota_client import IotaClient as ShimmerClient

env = environ.Env()
# Create an ShimmerClient instance
client = ShimmerClient({"nodes": ["https://shimmer.naerd.tech"]})

# Set the path for the wallet data
base_path = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
wallet_path = os.path.join(base_path, "database")

# Load the stronghold_password and shimmer_mnemonic values from the config.json file
stronghold_password = env("STRONGHOLD_PASSWORD")
shimmer_mnemonic = env("SHIMMER_MNEMONIC")


#####################################
# SHIMMER SECTION
#####################################


# Option one create a Shimmer profile
async def CreateShimmerProfile():
    # Check if wallet.stronghold exists and exit if present
    if os.path.isfile(f"{wallet_path}/database/wallet.stronghold"):
        logger.debug("Profile already exists. Choose a different option.")
    else:
        print("Creating new profile")
        # This creates a new database and account

        client_options = {
            "nodes": ["https://api.testnet.shimmer.network"],
        }

        # Shimmer coin type
        coin_type = 4219

        # Use the wallet_path for the wallet.stronghold file
        secret_manager = iota_wallet.StrongholdSecretManager(
            os.path.join(wallet_path, "wallet.stronghold"), stronghold_password
        )

        # Use the wallet_path for the nftmanager-database directory
        wallet = iota_wallet.IotaWallet(
            os.path.join(wallet_path, "nftmanager-database"),
            client_options,
            coin_type,
            secret_manager,
        )

        # Store the mnemonic in the Stronghold snapshot, this only needs to be done once
        account = wallet.store_mnemonic(shimmer_mnemonic)

        account = wallet.create_account("NFTManager")
        logger.info(account)


async def GetShimmerAddresses():
    # This shows all addresses in an account
    wallet = iota_wallet.IotaWallet("./nftmanager-database")
    account = wallet.get_account("NFTManager")
    address = account.addresses()
    logger.info(f"Address: {address}")


# def SendNativeToken():
#     if IsConfigDone() == True:
#         # Send native tokens
#         logger.info("I am in Send native tokens")
#         wallet = IotaWallet('./nftmanager-database')
#         account = wallet.get_account('NFTManager')

#         # Sync account with the node
#         response = account.sync_account()
#         logger.info("Account syncronized")
#         logger.info("Sending to " + str(shimmer_receiver_address))
#         wallet.set_stronghold_password(stronghold_password)

#         outputs = [{
#             "address": shimmer_receiver_address,
#             "nativeTokens": [(
#                 shimmer_native_token_id,
#                 # hex converted
#                 hex(int(shimmer_native_token_amount))
#             )],
#         }];

#         transaction = account.send_native_tokens(outputs, None)

#         logger.info("Transaction sent")
#         logger.info("Sleeping 25s to make sure transaction is out")
#         time.sleep(25)

# def SendToList():
#     global shimmer_receiver_address
#     if IsConfigDone() == True:
#         if os.path.isfile('wallet.stronghold') == False:
#             print("Profile does not exists. Please create a profile first.")
#         else:
#          ReadAddressesFromFile()

# def ReadAddressesFromFile():
#     global shimmer_receiver_address
#     global shimmer_address_sent_to_filename
#     if IsConfigDone() == True:
#         with open('addresses_to_send.txt', mode ='r', encoding='UTF8') as file:
#             for line in file.readlines():
#                 shimmer_reply_address = re.findall(shimmer_address_pattern, line, flags=re.IGNORECASE)
#                 for shimmer_receiver_address in shimmer_reply_address:
#                     if line.startswith(shimmer_receiver_address):
#                         logger.info("Address found " + str(shimmer_receiver_address))
#                         SendNativeToken()
#                         shimmer_receiver_address
#                         WriteToFile(shimmer_receiver_address, shimmer_address_sent_to_filename)
#                     else:
#                         logger.info("This is not an address")
