""""
Copyright © Krypton 2022 - https://github.com/kkrypt0nn (https://krypton.ninja)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""

import json
from collections.abc import Callable
from typing import TypeVar

import environ
from discord.ext import commands
from exceptions import UserBlacklisted, UserNotCampaignManager, UserNotOwner, UserSetup
from helpers import db_manager
from iota_client import (
    IotaClient as ShimmerClient,  # Imported as ShimmerClient to prevent confusion
)

env = environ.Env()
shimmer_client = ShimmerClient()  # Initialize ShimmerClient

T = TypeVar("T")


def debug_server() -> bool:
    """
    This is a custom check to see we are running on the debug server.
    """
    debug_mode = env.bool("DEBUG_MODE", True)
    if debug_mode == "True":
        debug_mode = True
    else:
        debug_mode = False
    debug_mode = bool(debug_mode)
    return debug_mode


def network_type_variable() -> str:
    """
    This function returns mainnet or devnet based on the debug status
    """
    if debug_server():
        tangle_network = "DevNet"
    else:
        tangle_network = "Mainnet"
    return tangle_network


def is_owner() -> Callable[[T], T]:
    """
    This is a custom check to see if the user executing the command is an owner of the bot.
    """

    async def predicate(context: commands.Context) -> bool:
        owners = [int(owner) for owner in env("OWNERS").split(",")]
        if context.author.id not in owners:
            raise UserNotOwner
        return True

    return commands.check(predicate)


def is_campaignmanager() -> Callable[[T], T]:
    """
    This is a custom check to see if the user executing the command is a campaign manager.
    """

    async def predicate(context: commands.Context) -> bool:
        with open("discord_bot/config.json") as file:
            data = json.load(file)
        if context.author.id not in data["campaign_manager"]:
            raise UserNotCampaignManager
        return True

    return commands.check(predicate)


def not_blacklisted() -> Callable[[T], T]:
    """
    This is a custom check to see if the user executing the command is blacklisted.
    """

    async def predicate(context: commands.Context) -> bool:
        if await db_manager.is_blacklisted(context.author.id):
            raise UserBlacklisted
        return True

    return commands.check(predicate)


def user_not_set_up() -> Callable[[T], T]:
    """
    This is a custom check to see if the user executing the command is blacklisted.
    """

    async def predicate(context: commands.Context) -> bool:
        if await db_manager.discord_user_setup_read(
            context.author.id, context.guild.id
        ):
            raise UserSetup
        return True

    return commands.check(predicate)


async def checkShimmerAddressFormat(address: str) -> Callable[[T], T]:
    """
    This is a custom check to see if the address is a Shimmer mainnet address.
    """
    # Verify if the address variable is valid according to the client libraries
    return shimmer_client.is_address_valid(address)


async def checkShimmerMainnetAddressFormat(address: str) -> Callable[[T], T]:
    """
    This is a custom check to see if the address is a Shimmer mainnet address.
    """
    # Verify if the address variable is valid according to the client libraries
    return shimmer_client.is_address_valid(address).startswith("smr1")


async def checkShimmerDevnetAddressFormat(address: str) -> Callable[[T], T]:
    """
    This is a custom check to see if the address is a Shimmer devnet address.
    """
    # Verify if the address variable is valid according to the client libraries
    return shimmer_client.is_address_valid(address).startswith("rms1")


async def checkUserHasShimmerMainnetAddress(
    user_id: int, guild_id: int
) -> Callable[[T], T]:
    """
    This is a custom check to see if the user already has a Shimmer mainnet address saved in the DB.
    """
    # Get the list of DiscordUserShimmerMainnetAddresses objects for the user
    addresses = await db_manager.user_has_dlt_address(user_id, guild_id)
    # Check if any of the objects has a non-empty shimmer_mainnet_address field
    for address in addresses:
        if address.shimmer_mainnet_address:
            return address.shimmer_mainnet_address
    return False


async def checkUserHasShimmerDevnetAddress(user_id: int, guild_id: int) -> bool | str:
    """
    This is a custom check to see if the user already has a Shimmer dev address saved in the DB.
    """
    # Get the list of DiscordUserShimmerDevnetAddresses objects for the user
    addresses = await db_manager.user_has_dlt_address(user_id, guild_id)
    # Check if any of the objects has a non-empty shimmer_devnet_address field
    for address in addresses:
        if address.shimmer_devnet_address:
            return address.shimmer_devnet_address
    return False
