""""
Copyright © Krypton 2022 - https://github.com/kkrypt0nn (https://krypton.ninja)
Description:
This is a template to create your own discord bot in python.

Version: 5.4
"""
import datetime
import pickle
import time
import traceback

import discord
import environ
import pandas as pd
import requests
from helpers import db_manager
from helpers.logger import logger
from iota_client import IotaClient as ShimmerClient

env = environ.Env()

# Create an ShimmerClient instance
client = ShimmerClient({"nodes": ["https://shimmer.naerd.tech"]})


# Get the node info
async def get_node_info():
    node_info = client.get_info()
    logger.info(f"{node_info}")


async def get_shimmer_ledger_state():
    # https://shimmer.naerd.tech/api/indexer/v2/blocks/0x64031faf6284682cb8badb62f7a1946fdf1be0656945dd255030d8246e5f2d5c/metadata
    try:
        logger.info("Getting Shimmer ledger state")
        api_route = "https://shimmer.naerd.tech/api/indexer/v1/outputs/basic"
        # param_list = ['hasNativeTokens=true', 'validated=true', 'pageSize=1']
        param_list = ["validated=true", "pageSize=1000", "hasExpiration=true"]
        api_parameters = f'{api_route}?{"&".join(param_list)}'
        jwt_token = env("SHIMMER_HORNET_JWT_TOKEN")

        head = {"Authorization": f"Bearer {jwt_token}"}
        # headers = {"content-type": "application/json"}

        ledger_state = {}
        cursor = None
        last_cursor = None
        await db_manager.clean_shimmer_ledger()
        while True:
            if cursor:
                api_parameters = (
                    api_parameters.split("&cursor=")[0] + f"&cursor={cursor}"
                )
                logger.debug(f"api_parameters: {api_parameters}")
            try:
                response = requests.get(url=api_parameters, headers=head)
                data = response.json()
                output_ids = data["items"]

                try:
                    # Get the outputs by their id
                    outputs = client.get_outputs(output_ids)
                    if outputs == "{'type': 'healthyNodePoolEmpty'}":
                        logger.debug("healthyNodePoolEmpty")
                        time.sleep(15)
                        continue
                    try:
                        for output_response in outputs:
                            try:
                                output = output_response["output"]
                                try:
                                    pubKeyHash = output["unlockConditions"][0][
                                        "address"
                                    ]["pubKeyHash"]
                                    value = int(output["amount"])
                                    address = pubKeyHash
                                    if address in ledger_state:
                                        ledger_state[address] += value
                                    else:
                                        ledger_state[address] = value

                                except KeyError:
                                    logger.debug("KeyError pubKeyHash not found")
                                    logger.debug("output")
                                    # logger.debug(output)
                                    logger.debug("output_ids")
                                    # logger.debug(output_ids)
                                    # 'pubKeyHash' key not present in output, skipping
                                    continue

                            except TypeError:
                                if output_response == {"type": "healthyNodePoolEmpty"}:
                                    time.sleep(15)
                                    continue
                                else:
                                    # 'output error', skipping
                                    logger.debug("TypeError output error")
                                    logger.debug("output")
                                    # logger.debug(output)
                                    logger.debug("output_ids")
                                    # logger.debug(output_ids)
                                    raise Exception("Unexpected output_response")

                        if "cursor" in data:
                            cursor = data["cursor"]

                        if cursor == last_cursor:
                            return
                            # continue
                        last_cursor = cursor

                    except Exception:
                        logger.info(traceback.format_exc())

                    logger.debug("Saving Shimmer ledger state to database")
                    # logger.debug(ledger_state)
                    await db_manager.add_shimmer_ledger(data=ledger_state)
                    time.sleep(3)

                except Exception:
                    logger.info(traceback.format_exc())

            except Exception:
                logger.info(traceback.format_exc())

    except Exception:
        logger.info(traceback.format_exc())


async def get_bech32_address_format_iota(ed25519_address):
    bech32_address = client.hex_to_bech32(ed25519_address, "smr")
    logger.info(bech32_address)
    logger.info("bech32_address")
    return bech32_address


async def save_shimmer_rich_list():
    try:
        logger.info("Saving Shimmer rich list to database")
        rows = await db_manager.get_shimmer_ledger()
        sorted_addresses = sorted(rows, key=lambda x: x.balance, reverse=True)
        top_addresses = sorted_addresses[:20]
        # Convert addresses to bech32 format using map function
        # top_addresses = list(map(lambda x: (client.hex_to_bech32(x[0], "smr"), x[1]), top_addresses))
        top_addresses = map(
            lambda x: (client.hex_to_bech32(x.address, "smr"), x.balance), top_addresses
        )
        # top_addresses = map(
        #     lambda x: (client.hex_to_bech32(x[0], "smr"), x[1]), top_addresses
        # )
        await db_manager.add_shimmer_top_addresses(data=top_addresses)
    except Exception:
        logger.info(traceback.format_exc())


async def prepare_shimmer_embed():
    logger.info("Preparing Shimmer rich list embed")
    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
    richlist_from_db = await db_manager.get_shimmer_top_addresses()
    logger.debug(f"richlist_from_db: {richlist_from_db}")
    complete_richlist = []
    for row in richlist_from_db:
        complete_richlist.append(f"{row[0]} - {row[1]}")
        try:
            # Here we create an embed with the title "Shimmer Richlist"
            embed = discord.Embed(title="🫰 Shimmer Top 5 Richlist", color=0x00FF00)
            for i in range(5):
                if i >= len(complete_richlist):
                    break
                address, balance = complete_richlist[i].split(" - ")
                if int(balance) >= 10**6:
                    balance = f"{float(balance)/10**6:.2f} SMR"
                else:
                    balance = f"{float(balance):.2f} Glow"
                embed.add_field(name=f"Top Address {i+1}", value=address)
                embed.add_field(name="Balance", value=balance)
                embed.add_field(name="", value="", inline=False)

            embed.add_field(name="Updates: ", value="Every 24h")
            embed.add_field(name="Last Update: ", value=current_time)
            with open("assets/embed_shimmer_richlist.pkl", "wb") as f:
                pickle.dump(embed, f)

        except Exception:
            logger.info(traceback.format_exc())

    logger.info("Shimmer richlist embed created")


async def create_shimmer_distribution_message():
    ledger_state = await db_manager.get_shimmer_ledger()
    # define the bin edges and labels
    bin_edges = [
        0,
        0.009,
        0.09,
        0.9,
        9,
        99,
        999,
        9999,
        99999,
        999999,
        9999999,
        99999999,
        999999999,
    ]
    labels = [
        "0 - 0.01",
        "0.01 - 0.1",
        "0.1 - 1",
        "1 - 10",
        "10 - 100",
        "100 - 1,000",
        "1,000 - 10,000",
        "10,000 - 100,000",
        "100,000 - 1,000,000",
        "1,000,000 - 10,000,000",
        "10,000,000 - 100,000,000",
        "100,000,000 - 1,000,000,000",
    ]

    # load the data into a pandas DataFrame
    ledger_df = pd.DataFrame(ledger_state, columns=["address", "balance"])
    ledger_df["balance"] = ledger_df["balance"] / 1000000

    # add a new column 'range' to the DataFrame that assigns a label to each address based on its balance
    ledger_df["Range"] = pd.cut(ledger_df["balance"], bins=bin_edges, labels=labels)

    # group the DataFrame by the 'range' column and calculate the number of addresses, sum of balances,
    # and percentage of total supply
    summary_table = (
        ledger_df.groupby("Range")
        .agg({"address": "count", "balance": "sum"})
        .rename(columns={"address": "Addresses", "balance": "Sum balances"})
    )

    # Add a new column 'Sum balances (original)' to store the original numeric values
    summary_table["Sum balances (original)"] = summary_table["Sum balances"]
    summary_table["Sum balances"] = summary_table["Sum balances"].apply(
        lambda x: f"{x:.2f}"
    )

    # Calculate the '% Addresses' column using the original numeric values in the 'Addresses' column
    summary_table["% Addresses"] = (
        summary_table["Addresses"] / summary_table["Addresses"].sum() * 100
    ).round(2)
    summary_table["% Addresses"] = summary_table["% Addresses"].apply(lambda x: f"{x}%")

    # Calculate the '% Supply' column using the original numeric values in the 'Sum balances (original)' column
    summary_table["% Supply"] = (
        summary_table["Sum balances (original)"]
        / summary_table["Sum balances (original)"].sum()
        * 100
    ).round(2)
    summary_table["% Supply"] = summary_table["% Supply"].apply(lambda x: f"{x}%")

    # Remove the 'Sum balances (original)' column
    summary_table.drop(columns=["Sum balances (original)"], inplace=True)

    # print the summary table
    print(summary_table)

    # Prepare the embed message
    logger.info("Preparing Shimmer distribution message")

    try:
        msg = "**Shimmer token distribution**\n\n"
        msg += "```"

        col_widths = [
            len(col)
            for col in [
                "100,000,000 - 1,000,000,000",
                "Addresses",
                "Sum balances",
                "%Addresses",
                "%Supply",
            ]
        ]
        for row in summary_table.itertuples():
            for i in range(len(col_widths)):
                if i + 1 < len(row):
                    col_widths[i] = max(len(str(row[i + 1])), col_widths[i])

        # build the table header
        header = "|"
        for width in col_widths:
            header += " " + "-" * width + " |"
        msg += header + "\n"

        # build the table header row
        header_row = "|"
        header_row += " " + "Range".ljust(col_widths[0]) + " |"
        header_row += " " + "Addresses".ljust(col_widths[1]) + " |"
        header_row += " " + "Sum balances".ljust(col_widths[2]) + " |"
        header_row += " " + "%Addresses".ljust(col_widths[3]) + " |"
        header_row += " " + "%Supply".ljust(col_widths[4]) + " |"
        msg += header_row + "\n"
        msg += header + "\n"

        # build the table data rows
        for row in summary_table.itertuples():
            data_row = "|"
            # data_row += " " + str(row[0]).rjust(col_widths[0]) + " |"
            data_row += " " + str(row[0]).rjust(col_widths[0]) + " |"
            data_row += " " + str(row[1]).rjust(col_widths[1]) + " |"
            data_row += " " + str(row[2]).rjust(col_widths[2]) + " |"
            data_row += " " + str(row[3]).rjust(col_widths[3]) + " |"
            data_row += " " + str(row[4]).rjust(col_widths[4]) + " |"
            msg += data_row + "\n"
        msg += header + "\n"

        msg += "```"
        msg += "**Data from Shimmer Ledger**"

        print(msg)
        with open("assets/message_shimmer_distribution.pkl", "wb") as f:
            pickle.dump(msg, f)

    except Exception:
        logger.info(traceback.format_exc())


async def main():
    # await get_shimmer_ledger_state()
    await save_shimmer_rich_list()
    await prepare_shimmer_embed()
    await create_shimmer_distribution_message()


if __name__ == "__main__":
    main()
