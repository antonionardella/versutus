# # This is the template of the confirmation window that expects the user to press confirm or cancel before continuing
# view = ux_items.Confirm()
# embed = discord.Embed(title="Please confirm that you want to...", color=0x64B5F6)
# embed.add_field(name="Privacy Policy ", value="https://privacy.policy")
# embed.add_field(name="Portal ", value="https://my.profile")
# await context.send(embed=embed, view=view, ephemeral=True)
# await view.wait()

# if view.value is None:
#     logger.debug(f"Timed out...")
#     await context.send("Timed out...")
#     return
# elif view.value:
#     logger.debug(f"Confirmed...")
#     return
# else:
#     logger.debug(f"Cancelled...")
#     await context.send("Cancelled, call the /usersetup command again.")
#     return
